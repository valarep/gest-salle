﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GestSalle.Classes;

namespace GestSalle
{
    public partial class Form1 : Form
    {
        private List<Salle> salles;
        private List<Groupe> groupes;
        private List<Employe> employes;
        private List<Poste> postes;
        private List<Label> labels = new List<Label>();

        private BindingSource bsSalles = new BindingSource();
        private BindingSource bsGroupes = new BindingSource();
        private BindingSource bsEmployes = new BindingSource();

        public Form1()
        {
            InitializeComponent();
            LabelsInit();
        }

        private void LabelsInit()
        {
            foreach(Control control in this.Controls)
            {
                if (control is Panel)
                {
                    foreach(Control innerControl in control.Controls)
                    {
                        if (innerControl is GroupBox groupBox)
                        {
                            Label label = (Label)groupBox.Controls[0];
                            labels.Add(label);
                        }
                    }
                }
            }
            labels = labels.OrderBy(l => l.Name).ToList();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            salles = Salle.GetAll();
            groupes = Groupe.GetAll();

            cmbSalles.DataSource = bsSalles;
            bsSalles.DataSource = salles;
            cmbSalles.DisplayMember = "Nom";

            cmbGroupes.DataSource = bsGroupes;
            bsGroupes.DataSource = groupes;
            cmbGroupes.DisplayMember = "Nom";

            lstEmployes.DataSource = bsEmployes;
            bsEmployes.DataSource = employes;
            lstEmployes.DisplayMember = "NomComplet";

            cmbSalles.SelectedIndexChanged += cmbSalles_SelectedIndexChanged;
            cmbSalles_SelectedIndexChanged(cmbSalles, EventArgs.Empty);
        }

        private void LoadEmployesFromGroupe()
        {
            employes = Employe.GetAll(((Groupe)cmbGroupes.SelectedValue).Id);
            bsEmployes.DataSource = employes;
        }

        private void cmbGroupes_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadEmployesFromGroupe();
            LoadPlacement();
        }

        private void lstEmployes_MouseDown(object sender, MouseEventArgs e)
        {
            if (lstEmployes.SelectedItem != null)
            {
                lstEmployes.Tag = lstEmployes.SelectedItem;
                DragDropEffects result = lstEmployes.DoDragDrop(lstEmployes, DragDropEffects.Move);
                if (result == DragDropEffects.Move)
                {
                    bsEmployes.Remove(lstEmployes.SelectedItem);
                }
            }

        }

        private void Label_MouseDown(object sender, MouseEventArgs e)
        {
            Label label = (Label)sender;
            if (label.Tag != null)
            {
                DragDropEffects result = label.DoDragDrop(label, DragDropEffects.Move);
                if (result == DragDropEffects.Move)
                {
                    label.Text = "";
                    label.Tag = null;
                }
            }
        }

        private void Label_DrapEnter(object sender, DragEventArgs e)
        {
            Control from = null;
            if (e.Data.GetDataPresent(typeof(ListBox)))
            {
                from = (Control)e.Data.GetData(typeof(ListBox));
            }
            else if (e.Data.GetDataPresent(typeof(Label)))
            {
                from = (Control)e.Data.GetData(typeof(Label));
            }
            Control to = (Control)sender;
            if (from != null && from != to && from.Tag is Employe && to.Parent.Tag != null && to.Tag == null)
            {
                e.Effect = DragDropEffects.Move;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void lstEmployes_DragEnter(object sender, DragEventArgs e)
        {
            Control from = null;
            if (e.Data.GetDataPresent(typeof(ListBox)))
            {
                from = (Control)e.Data.GetData(typeof(ListBox));
            }
            else if (e.Data.GetDataPresent(typeof(Label)))
            {
                from = (Control)e.Data.GetData(typeof(Label));
            }
            Control to = (Control)sender;
            if (from != null && from != to && from.Tag is Employe)
            {
                e.Effect = DragDropEffects.Move;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void Label_DrapDrop(object sender, DragEventArgs e)
        {
            Label label = (Label)sender;
            Control from = null;
            if (e.Data.GetDataPresent(typeof(ListBox)))
            {
                from = (Control)e.Data.GetData(typeof(ListBox));
            }
            else if (e.Data.GetDataPresent(typeof(Label)))
            {
                from = (Control)e.Data.GetData(typeof(Label));
            }
            Employe employe = (Employe)from.Tag;
            AddemployeInLabel(employe, label);
        }

        private void AddemployeInLabel(Employe employe, Label label)
        {
            label.Tag = employe;
            label.Text = employe.NomComplet;
        }

        private void lstEmployes_DragDrop(object sender, DragEventArgs e)
        {
            Control from = null;
            if (e.Data.GetDataPresent(typeof(ListBox)))
            {
                from = (Control)e.Data.GetData(typeof(ListBox));
            }
            else if (e.Data.GetDataPresent(typeof(Label)))
            {
                from = (Control)e.Data.GetData(typeof(Label));
            }
            Employe employe = (Employe)from.Tag;
            bsEmployes.Add(employe);
            employes = employes.OrderBy(emp => emp.Nom).ThenBy(emp => emp.Prénom).ToList();
            bsEmployes.DataSource = employes;
        }

        private void cmbSalles_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadEmployesFromGroupe();
            LoadPlacement();
        }

        private void LoadPlacement()
        {
            Salle salle = (Salle)cmbSalles.SelectedItem;
            Groupe groupe = (Groupe)cmbGroupes.SelectedItem;
            postes = Poste.GetAll(salle.Id);

            int index = 0;


            foreach (Label label in labels)
            {
                if (index < postes.Count)
                {
                    Poste poste = postes[index];
                    label.Parent.Text = poste.Nom;
                    label.Parent.Tag = poste;
                    int? id = Employe.Get(new Placement { Groupe = groupe, Salle = salle, Poste = poste })?.Id;
                    Employe employe = (id != null) ? employes.Find(emp => emp.Id == id) : null;
                    if (employe != null)
                    {
                        AddemployeInLabel(employe, label);
                        bsEmployes.Remove(employe);
                    }
                    else
                    {
                        label.Text = "";
                        label.Tag = null;
                    }
                }
                else
                {
                    label.Parent.Text = "";
                    label.Parent.Tag = null;
                }
                index++;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            Groupe groupe = (Groupe)cmbGroupes.SelectedItem;
            Salle salle = (Salle)cmbSalles.SelectedItem;

            Dictionary<Poste, Employe> emplacements = new Dictionary<Poste, Employe>();
            foreach(Label label in labels)
            {
                Poste poste = (Poste)label.Parent.Tag;
                if (poste != null)
                {
                    if (label.Tag != null)
                    {
                        Employe employe = (Employe)label.Tag;
                        emplacements.Add(poste, employe);
                    }
                }
            }
            Placement.Save(groupe, salle, emplacements);
        }

        private void ResetPlacement()
        {
            foreach (Label label in labels)
            {
                label.Text = "";
                label.Tag = null;
            }
        }

        private void BtnReset_Click(object sender, EventArgs e)
        {
            LoadEmployesFromGroupe();
            ResetPlacement();
        }
    }
}
