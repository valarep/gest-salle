﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestSalle.Classes;
using GestSalle.Dal;

namespace GestSalle.Dao
{
    public static class GroupeDao
    {
        public static List<Groupe> GetAll()
        {
            List<Groupe> items = new List<Groupe>();
            MySqlConnection connection = AbstractDao.Connection;
            connection.Open();
            string query = "SELECT * FROM `groupe`";
            MySqlCommand command = AbstractDao.Connection.CreateCommand();
            command.CommandText = query;
            command.Prepare();
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                items.Add(new Groupe
                {
                    Id = (int)reader["id"],
                    Nom = (string)reader["nom"]
                });
            }
            reader.Close();
            connection.Close();
            return items;
        }

        public static List<Groupe> GetAll(int id_etudiant)
        {
            List<Groupe> items = new List<Groupe>();
            MySqlConnection connection = AbstractDao.Connection;
            connection.Open();
            string query = "SELECT * " +
                            "FROM `groupe` " +
                            "INNER JOIN `etudiant_groupe` ON `groupe`.`id` = `etudiant_groupe`.`id_groupe` " +
                            "WHERE `id_etudiant` = ?id_etudiant;";
            MySqlCommand command = AbstractDao.Connection.CreateCommand();
            command.CommandText = query;
            command.Prepare();
            command.Parameters.Add(new MySqlParameter("id_etudiant", id_etudiant));
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                items.Add(new Groupe
                {
                    Id = (int)reader["id"],
                    Nom = (string)reader["nom"]
                });
            }
            reader.Close();
            connection.Close();
            return items;
        }
    }
}
