﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestSalle.Classes;
using GestSalle.Dal;

namespace GestSalle.Dao
{
    public class PosteDao
    {
        public static List<Poste> GetAll(int id_salle)
        {
            List<Poste> items = new List<Poste>();
            MySqlConnection connection = AbstractDao.Connection;
            connection.Open();
            string query = "SELECT * FROM `poste` " +
                            "WHERE `id_salle` = ?id_salle";
            MySqlCommand command = AbstractDao.Connection.CreateCommand();
            command.CommandText = query;
            command.Prepare();
            command.Parameters.Add(new MySqlParameter("id_salle", id_salle));
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                items.Add(new Poste
                {
                    Id = (int)reader["id"],
                    Nom = (string)reader["nom"]
                });
            }
            reader.Close();
            connection.Close();
            return items;
        }
    }
}
