﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestSalle.Classes;
using GestSalle.Dal;

namespace GestSalle.Dao
{
    public static class EmployeDao
    {
        public static List<Employe> GetAll(int id_groupe)
        {
            List<Employe> items = new List<Employe>();
            MySqlConnection connection = AbstractDao.Connection;
            connection.Open();
            string query = "SELECT * " +
                            "FROM `employe` " +
                            "INNER JOIN `employe_groupe` ON `employe`.`id` = `employe_groupe`.`id_employe` " +
                            "WHERE `id_groupe` = ?id_groupe;";
            MySqlCommand command = AbstractDao.Connection.CreateCommand();
            command.CommandText = query;
            command.Prepare();
            command.Parameters.Add(new MySqlParameter("id_groupe", id_groupe));
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                items.Add(new Employe
                {
                    Id = (int)reader["id"],
                    Prénom = (string)reader["prenom"],
                    Nom = (string)reader["nom"],
                });
            }
            reader.Close();
            connection.Close();
            return items;
        }

        public static Employe Get(Placement placement)
        {
            Employe item = null;
            MySqlConnection connection = AbstractDao.Connection;
            connection.Open();
            string query = "SELECT `employe`.* " +
                            "FROM `employe` " +
                            "INNER JOIN `groupe_salle_poste` ON `groupe_salle_poste`.`id_employe` = `employe`.`id` " +
                            "WHERE `id_groupe` = ?id_groupe " +
                            "AND `id_salle` = ?id_salle " +
                            "AND `id_poste` = ?id_poste;";
            MySqlCommand command = AbstractDao.Connection.CreateCommand();
            command.CommandText = query;
            command.Prepare();
            command.Parameters.Add(new MySqlParameter("id_groupe", placement.Groupe.Id));
            command.Parameters.Add(new MySqlParameter("id_salle", placement.Salle.Id));
            command.Parameters.Add(new MySqlParameter("id_poste", placement.Poste.Id));
            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                item = new Employe
                {
                    Id = (int)reader["id"],
                    Prénom = (string)reader["prenom"],
                    Nom = (string)reader["nom"]
                };
            }
            reader.Close();
            connection.Close();
            return item;
        }
    }
}