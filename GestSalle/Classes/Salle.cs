﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestSalle.Dao;

namespace GestSalle.Classes
{
    public class Salle
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public List<Poste> Postes { get; set; }

        public static List<Salle> GetAll() => SalleDao.GetAll();
    }
}
