﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestSalle.Dao;

namespace GestSalle.Classes
{
    public class Groupe
    {
        public int Id { get; set; }
        public string Nom { get; set; }

        public static List<Groupe> GetAll() => GroupeDao.GetAll();
        public static List<Groupe> GetAll(int id_etudiant) => GroupeDao.GetAll(id_etudiant);
    }
}
