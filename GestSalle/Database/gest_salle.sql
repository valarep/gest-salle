-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 10 mai 2019 à 13:37
-- Version du serveur :  5.7.21
-- Version de PHP :  7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `gest_salle`
--

-- --------------------------------------------------------

--
-- Structure de la table `employe`
--

DROP TABLE IF EXISTS `employe`;
CREATE TABLE IF NOT EXISTS `employe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prenom` varchar(30) NOT NULL,
  `nom` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `employe`
--

INSERT INTO `employe` (`id`, `prenom`, `nom`) VALUES
(1, 'Youssef', 'AIT LACHGUER'),
(2, 'Corentin', 'BON'),
(3, 'Thomas', 'COCHART'),
(4, 'Antoine', 'DANGLETERRE'),
(5, 'Sofiane', 'DEBRABANT'),
(6, 'Hugo', 'DELZENNE'),
(7, 'Alexandre', 'GENESTE'),
(8, 'Alexandre', 'GHESQUIERE'),
(9, 'Baptiste', 'LECLERCQ'),
(10, 'Mathieu', 'LISON'),
(11, 'Pierre', 'STAWIKOWSKI'),
(12, 'Thomas', 'THURLURE'),
(13, 'Vincent', 'Bregovic'),
(14, 'Mickaël', 'Cartier'),
(15, 'Alex', 'Chaval'),
(16, 'Stéphane', 'Coupé'),
(17, 'Dominique', 'Dargent'),
(18, 'Céline', 'De Pauw'),
(19, 'Tony', 'De Vito'),
(20, 'Tanguy', 'Defer'),
(21, 'Axelle', 'Delbauffe'),
(22, 'Asyl', 'Guillot'),
(23, 'Mickaël', 'Hyolle'),
(24, 'Béatrice', 'Kuzniewski'),
(25, 'Benjamin', 'Lorent');

-- --------------------------------------------------------

--
-- Structure de la table `employe_groupe`
--

DROP TABLE IF EXISTS `employe_groupe`;
CREATE TABLE IF NOT EXISTS `employe_groupe` (
  `id_employe` int(11) NOT NULL,
  `id_groupe` int(11) NOT NULL,
  PRIMARY KEY (`id_employe`,`id_groupe`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `employe_groupe`
--

INSERT INTO `employe_groupe` (`id_employe`, `id_groupe`) VALUES
(1, 2),
(2, 2),
(3, 2),
(4, 2),
(5, 2),
(6, 2),
(7, 2),
(8, 2),
(9, 2),
(10, 2),
(11, 2),
(12, 2),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1);

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

DROP TABLE IF EXISTS `groupe`;
CREATE TABLE IF NOT EXISTS `groupe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `groupe`
--

INSERT INTO `groupe` (`id`, `nom`) VALUES
(1, 'DWWM-18-19'),
(2, 'CDI-18-19');

-- --------------------------------------------------------

--
-- Structure de la table `groupe_salle_poste`
--

DROP TABLE IF EXISTS `groupe_salle_poste`;
CREATE TABLE IF NOT EXISTS `groupe_salle_poste` (
  `id_groupe` int(11) NOT NULL,
  `id_salle` int(11) NOT NULL,
  `id_poste` int(11) NOT NULL,
  `id_employe` int(11) NOT NULL,
  PRIMARY KEY (`id_groupe`,`id_salle`,`id_poste`),
  KEY `id_etudiant` (`id_employe`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `groupe_salle_poste`
--

INSERT INTO `groupe_salle_poste` (`id_groupe`, `id_salle`, `id_poste`, `id_employe`) VALUES
(2, 2, 14, 9),
(2, 2, 15, 3),
(2, 2, 16, 8),
(2, 2, 17, 2),
(2, 2, 18, 12),
(2, 2, 19, 11),
(2, 2, 20, 4),
(2, 2, 21, 7),
(2, 2, 22, 6),
(2, 2, 23, 5),
(2, 2, 25, 1),
(2, 2, 26, 10);

-- --------------------------------------------------------

--
-- Structure de la table `poste`
--

DROP TABLE IF EXISTS `poste`;
CREATE TABLE IF NOT EXISTS `poste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(7) NOT NULL,
  `id_salle` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_salle` (`id_salle`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `poste`
--

INSERT INTO `poste` (`id`, `nom`, `id_salle`) VALUES
(1, 'S202P01', 1),
(2, 'S202P02', 1),
(3, 'S202P03', 1),
(4, 'S202P04', 1),
(5, 'S202P05', 1),
(6, 'S202P06', 1),
(7, 'S202P07', 1),
(8, 'S202P08', 1),
(9, 'S202P09', 1),
(10, 'S202P10', 1),
(11, 'S202P11', 1),
(12, 'S202P12', 1),
(13, 'S202P13', 1),
(14, 'S208P01', 2),
(15, 'S208P02', 2),
(16, 'S208P03', 2),
(17, 'S208P04', 2),
(18, 'S208P05', 2),
(19, 'S208P06', 2),
(20, 'S208P07', 2),
(21, 'S208P08', 2),
(22, 'S208P09', 2),
(23, 'S208P10', 2),
(24, 'S208P11', 2),
(25, 'S208P12', 2),
(26, 'S208P13', 2);

-- --------------------------------------------------------

--
-- Structure de la table `salle`
--

DROP TABLE IF EXISTS `salle`;
CREATE TABLE IF NOT EXISTS `salle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `salle`
--

INSERT INTO `salle` (`id`, `nom`) VALUES
(1, '202'),
(2, '208');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
