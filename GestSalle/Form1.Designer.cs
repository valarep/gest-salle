﻿namespace GestSalle
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label04 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label03 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label02 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label01 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label08 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label07 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label06 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label05 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label09 = new System.Windows.Forms.Label();
            this.lstEmployes = new System.Windows.Forms.ListBox();
            this.cmbSalles = new System.Windows.Forms.ComboBox();
            this.lblSalle = new System.Windows.Forms.Label();
            this.lblGroupe = new System.Windows.Forms.Label();
            this.cmbGroupes = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label04);
            this.groupBox4.Location = new System.Drawing.Point(89, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(80, 80);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "P04";
            // 
            // label04
            // 
            this.label04.AllowDrop = true;
            this.label04.Location = new System.Drawing.Point(6, 16);
            this.label04.Name = "label04";
            this.label04.Size = new System.Drawing.Size(68, 61);
            this.label04.TabIndex = 1;
            this.label04.DragDrop += new System.Windows.Forms.DragEventHandler(this.Label_DrapDrop);
            this.label04.DragEnter += new System.Windows.Forms.DragEventHandler(this.Label_DrapEnter);
            this.label04.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Label_MouseDown);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label03);
            this.groupBox3.Location = new System.Drawing.Point(175, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(80, 80);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "P03";
            // 
            // label03
            // 
            this.label03.AllowDrop = true;
            this.label03.Location = new System.Drawing.Point(6, 16);
            this.label03.Name = "label03";
            this.label03.Size = new System.Drawing.Size(68, 61);
            this.label03.TabIndex = 1;
            this.label03.DragDrop += new System.Windows.Forms.DragEventHandler(this.Label_DrapDrop);
            this.label03.DragEnter += new System.Windows.Forms.DragEventHandler(this.Label_DrapEnter);
            this.label03.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Label_MouseDown);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label02);
            this.groupBox2.Location = new System.Drawing.Point(261, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(80, 80);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "P02";
            // 
            // label02
            // 
            this.label02.AllowDrop = true;
            this.label02.Location = new System.Drawing.Point(6, 16);
            this.label02.Name = "label02";
            this.label02.Size = new System.Drawing.Size(68, 61);
            this.label02.TabIndex = 1;
            this.label02.DragDrop += new System.Windows.Forms.DragEventHandler(this.Label_DrapDrop);
            this.label02.DragEnter += new System.Windows.Forms.DragEventHandler(this.Label_DrapEnter);
            this.label02.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Label_MouseDown);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label01);
            this.groupBox1.Location = new System.Drawing.Point(347, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(80, 80);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "P01";
            // 
            // label01
            // 
            this.label01.AllowDrop = true;
            this.label01.Location = new System.Drawing.Point(6, 16);
            this.label01.Name = "label01";
            this.label01.Size = new System.Drawing.Size(68, 61);
            this.label01.TabIndex = 0;
            this.label01.DragDrop += new System.Windows.Forms.DragEventHandler(this.Label_DrapDrop);
            this.label01.DragEnter += new System.Windows.Forms.DragEventHandler(this.Label_DrapEnter);
            this.label01.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Label_MouseDown);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label08);
            this.groupBox8.Location = new System.Drawing.Point(89, 3);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(80, 80);
            this.groupBox8.TabIndex = 4;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "P08";
            // 
            // label08
            // 
            this.label08.AllowDrop = true;
            this.label08.Location = new System.Drawing.Point(6, 16);
            this.label08.Name = "label08";
            this.label08.Size = new System.Drawing.Size(68, 61);
            this.label08.TabIndex = 1;
            this.label08.DragDrop += new System.Windows.Forms.DragEventHandler(this.Label_DrapDrop);
            this.label08.DragEnter += new System.Windows.Forms.DragEventHandler(this.Label_DrapEnter);
            this.label08.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Label_MouseDown);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label07);
            this.groupBox7.Location = new System.Drawing.Point(175, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(80, 80);
            this.groupBox7.TabIndex = 3;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "P07";
            // 
            // label07
            // 
            this.label07.AllowDrop = true;
            this.label07.Location = new System.Drawing.Point(6, 16);
            this.label07.Name = "label07";
            this.label07.Size = new System.Drawing.Size(68, 61);
            this.label07.TabIndex = 1;
            this.label07.DragDrop += new System.Windows.Forms.DragEventHandler(this.Label_DrapDrop);
            this.label07.DragEnter += new System.Windows.Forms.DragEventHandler(this.Label_DrapEnter);
            this.label07.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Label_MouseDown);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label06);
            this.groupBox6.Location = new System.Drawing.Point(261, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(80, 80);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "P06";
            // 
            // label06
            // 
            this.label06.AllowDrop = true;
            this.label06.Location = new System.Drawing.Point(6, 16);
            this.label06.Name = "label06";
            this.label06.Size = new System.Drawing.Size(68, 61);
            this.label06.TabIndex = 1;
            this.label06.DragDrop += new System.Windows.Forms.DragEventHandler(this.Label_DrapDrop);
            this.label06.DragEnter += new System.Windows.Forms.DragEventHandler(this.Label_DrapEnter);
            this.label06.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Label_MouseDown);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label05);
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(80, 80);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "P05";
            // 
            // label05
            // 
            this.label05.AllowDrop = true;
            this.label05.Location = new System.Drawing.Point(6, 16);
            this.label05.Name = "label05";
            this.label05.Size = new System.Drawing.Size(68, 61);
            this.label05.TabIndex = 1;
            this.label05.DragDrop += new System.Windows.Forms.DragEventHandler(this.Label_DrapDrop);
            this.label05.DragEnter += new System.Windows.Forms.DragEventHandler(this.Label_DrapEnter);
            this.label05.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Label_MouseDown);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.groupBox10);
            this.panel3.Controls.Add(this.groupBox11);
            this.panel3.Location = new System.Drawing.Point(271, 106);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(174, 88);
            this.panel3.TabIndex = 3;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label10);
            this.groupBox10.Location = new System.Drawing.Point(89, 3);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(80, 80);
            this.groupBox10.TabIndex = 2;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "P10";
            // 
            // label10
            // 
            this.label10.AllowDrop = true;
            this.label10.Location = new System.Drawing.Point(6, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 61);
            this.label10.TabIndex = 1;
            this.label10.DragDrop += new System.Windows.Forms.DragEventHandler(this.Label_DrapDrop);
            this.label10.DragEnter += new System.Windows.Forms.DragEventHandler(this.Label_DrapEnter);
            this.label10.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Label_MouseDown);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label11);
            this.groupBox11.Location = new System.Drawing.Point(3, 3);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(80, 80);
            this.groupBox11.TabIndex = 3;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "P11";
            // 
            // label11
            // 
            this.label11.AllowDrop = true;
            this.label11.Location = new System.Drawing.Point(6, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 61);
            this.label11.TabIndex = 1;
            this.label11.DragDrop += new System.Windows.Forms.DragEventHandler(this.Label_DrapDrop);
            this.label11.DragEnter += new System.Windows.Forms.DragEventHandler(this.Label_DrapEnter);
            this.label11.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Label_MouseDown);
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.groupBox13);
            this.panel4.Controls.Add(this.groupBox12);
            this.panel4.Location = new System.Drawing.Point(271, 200);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(174, 88);
            this.panel4.TabIndex = 2;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.label13);
            this.groupBox13.Location = new System.Drawing.Point(3, 3);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(80, 80);
            this.groupBox13.TabIndex = 2;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "P13";
            // 
            // label13
            // 
            this.label13.AllowDrop = true;
            this.label13.Location = new System.Drawing.Point(6, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 61);
            this.label13.TabIndex = 1;
            this.label13.DragDrop += new System.Windows.Forms.DragEventHandler(this.Label_DrapDrop);
            this.label13.DragEnter += new System.Windows.Forms.DragEventHandler(this.Label_DrapEnter);
            this.label13.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Label_MouseDown);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.label12);
            this.groupBox12.Location = new System.Drawing.Point(89, 3);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(80, 80);
            this.groupBox12.TabIndex = 4;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "P12";
            // 
            // label12
            // 
            this.label12.AllowDrop = true;
            this.label12.Location = new System.Drawing.Point(6, 16);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 61);
            this.label12.TabIndex = 1;
            this.label12.DragDrop += new System.Windows.Forms.DragEventHandler(this.Label_DrapDrop);
            this.label12.DragEnter += new System.Windows.Forms.DragEventHandler(this.Label_DrapEnter);
            this.label12.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Label_MouseDown);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label09);
            this.groupBox9.Location = new System.Drawing.Point(3, 3);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(80, 80);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "P09";
            // 
            // label09
            // 
            this.label09.AllowDrop = true;
            this.label09.Location = new System.Drawing.Point(6, 16);
            this.label09.Name = "label09";
            this.label09.Size = new System.Drawing.Size(68, 61);
            this.label09.TabIndex = 1;
            this.label09.DragDrop += new System.Windows.Forms.DragEventHandler(this.Label_DrapDrop);
            this.label09.DragEnter += new System.Windows.Forms.DragEventHandler(this.Label_DrapEnter);
            this.label09.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Label_MouseDown);
            // 
            // lstEmployes
            // 
            this.lstEmployes.AllowDrop = true;
            this.lstEmployes.FormattingEnabled = true;
            this.lstEmployes.Location = new System.Drawing.Point(12, 64);
            this.lstEmployes.Name = "lstEmployes";
            this.lstEmployes.Size = new System.Drawing.Size(224, 355);
            this.lstEmployes.TabIndex = 6;
            this.lstEmployes.DragDrop += new System.Windows.Forms.DragEventHandler(this.lstEmployes_DragDrop);
            this.lstEmployes.DragEnter += new System.Windows.Forms.DragEventHandler(this.lstEmployes_DragEnter);
            this.lstEmployes.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lstEmployes_MouseDown);
            // 
            // cmbSalles
            // 
            this.cmbSalles.FormattingEnabled = true;
            this.cmbSalles.Location = new System.Drawing.Point(115, 12);
            this.cmbSalles.Name = "cmbSalles";
            this.cmbSalles.Size = new System.Drawing.Size(121, 21);
            this.cmbSalles.TabIndex = 7;
            // 
            // lblSalle
            // 
            this.lblSalle.AutoSize = true;
            this.lblSalle.Location = new System.Drawing.Point(12, 15);
            this.lblSalle.Name = "lblSalle";
            this.lblSalle.Size = new System.Drawing.Size(36, 13);
            this.lblSalle.TabIndex = 8;
            this.lblSalle.Text = "Salle :";
            // 
            // lblGroupe
            // 
            this.lblGroupe.AutoSize = true;
            this.lblGroupe.Location = new System.Drawing.Point(9, 42);
            this.lblGroupe.Name = "lblGroupe";
            this.lblGroupe.Size = new System.Drawing.Size(48, 13);
            this.lblGroupe.TabIndex = 9;
            this.lblGroupe.Text = "Groupe :";
            // 
            // cmbGroupes
            // 
            this.cmbGroupes.FormattingEnabled = true;
            this.cmbGroupes.Location = new System.Drawing.Point(115, 39);
            this.cmbGroupes.Name = "cmbGroupes";
            this.cmbGroupes.Size = new System.Drawing.Size(121, 21);
            this.cmbGroupes.TabIndex = 10;
            this.cmbGroupes.SelectedIndexChanged += new System.EventHandler(this.cmbGroupes_SelectedIndexChanged);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(839, 398);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Enregistrer";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(758, 398);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 12;
            this.btnReset.Text = "R-à-Z";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.BtnReset_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox5);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Location = new System.Drawing.Point(482, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(432, 88);
            this.panel1.TabIndex = 13;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.groupBox8);
            this.panel2.Controls.Add(this.groupBox6);
            this.panel2.Controls.Add(this.groupBox7);
            this.panel2.Controls.Add(this.groupBox9);
            this.panel2.Location = new System.Drawing.Point(568, 106);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(346, 88);
            this.panel2.TabIndex = 14;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(926, 433);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.cmbGroupes);
            this.Controls.Add(this.lblGroupe);
            this.Controls.Add(this.lblSalle);
            this.Controls.Add(this.cmbSalles);
            this.Controls.Add(this.lstEmployes);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.ListBox lstEmployes;
        private System.Windows.Forms.ComboBox cmbSalles;
        private System.Windows.Forms.Label lblSalle;
        private System.Windows.Forms.Label lblGroupe;
        private System.Windows.Forms.ComboBox cmbGroupes;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label01;
        private System.Windows.Forms.Label label04;
        private System.Windows.Forms.Label label03;
        private System.Windows.Forms.Label label02;
        private System.Windows.Forms.Label label08;
        private System.Windows.Forms.Label label07;
        private System.Windows.Forms.Label label06;
        private System.Windows.Forms.Label label05;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label09;
    }
}

